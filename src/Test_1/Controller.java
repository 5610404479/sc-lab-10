package Test_1;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Controller {

	ColorFrame frame;
	ActionListener list;
	Container contentPane;

	public static void main(String[] args) {
		new Controller();
	}

	public Controller() {
		frame = new ColorFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 400);
		frame.setVisible(true);
		frame.setTitle("My Colors");
		frame.setLocation(510,160);
		list = new ListenerMgr();
		frame.setListener(list);


	}

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == frame.bRed)
			    frame.colorPanel.setBackground(Color.RED);
			else if (e.getSource() == frame.bGreen)
		        frame.colorPanel.setBackground(Color.GREEN);
			else if (e.getSource() == frame.bBlue)
		        frame.colorPanel.setBackground( Color.blue);
			
			frame.repaint();

		}
	}
}
