package Test_1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeListener;

import Test_1.Controller.ListenerMgr;

public class ColorFrame extends JFrame {
 JPanel colorPanel;
	private JPanel buttonPanel;
	 JButton bRed, bGreen, bBlue;
	private TitledBorder l1;
	private Border board;

	public ColorFrame() {
		createPanel();
		createPanelButton();
		
	}

	public void createPanel() {
		colorPanel = new JPanel();
		buttonPanel = new JPanel();
		add(colorPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		
	}

	public void createPanelButton() {
		bRed = new JButton("Red");
		bGreen = new JButton("Green");
		bBlue = new JButton("Blue");
		buttonPanel.add(bRed);
		buttonPanel.add(bBlue);
		buttonPanel.add(bGreen);
		board = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		l1 = BorderFactory.createTitledBorder(board, "Color");
		buttonPanel.setBorder(l1);
		
	}
	public void setListener(ActionListener list) {
		bRed.addActionListener(list);
		bGreen.addActionListener(list);
		bBlue.addActionListener(list);

	}

	
	

	
}
