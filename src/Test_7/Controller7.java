package Test_7;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;

public class Controller7 {
	view frame;
	ActionListener list;
	BankAccount model;
	ArrayList<String> save;

	public void deposite(double amount) {
		model.deposit(amount);
	}

	public void withdraw(double amount) {
		model.withdraw(amount);
	}

	public double getBalance() {
		return model.getBalance();
	}

	public static void main(String[] args) {
		new Controller7();
	}

	public Controller7() {
		frame = new view();
		model = new BankAccount();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 320);
		frame.setVisible(true);
		frame.setTitle("Transaction");
		frame.setLocation(510, 160);
		list = new ListenerMgr();
		frame.setListener(list);
		save = new ArrayList<String>();

	}

	class ListenerMgr implements ActionListener {
		int total = 0;
		String b = " ";

		@Override
		public void actionPerformed(ActionEvent e) {
			String str = frame.text1.getText();
			int input = Integer.parseInt(str);
			if (frame.btDeposit == e.getSource()) {
				model.deposit(input);
				total = (int) model.getBalance();
				save.add("Total Deposite :" + " " + str + "\n" + " "
						+ "Balance :" + " " + total + "\n");

			}
			if (frame.btWithdraw == e.getSource()) {
				model.withdraw(input);
				total = (int) model.getBalance();
				save.add("Total Withdraw :" + " " + str + "\n" + " "
						+ "Balance :" + " " + total + "\n ");

			}
			for (int i = 1; i < save.size() - 1; i++) {
				String a = save.get(i - 1);
				String c = save.get(i);
				String d = save.get(i + 1);
				frame.area1.setText(a + c + d);

			}

		
		}
	}
}
