package Test_5;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.MenuListener;

public class ColorFrame5 extends JFrame {
	JPanel colorPanel, menuPanel;
	JMenuBar mainMenu;
	JMenu menu, menu2;
	JMenuItem itemRed, itemBlue, itemGreen;
	private Border board;

	public ColorFrame5() {
		createPanel();
		createMenuBar();
	}

	public void createPanel() {
		colorPanel = new JPanel();
		menuPanel = new JPanel();
		add(menuPanel);
		add(colorPanel);
		board = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		menuPanel.setBorder(board);

	}

	public void createMenuBar() {
		// Menu Bar
		mainMenu = new JMenuBar();

		// Menu1
		menu = new JMenu("Select");
		itemRed = new JMenuItem("Red");
		itemBlue = new JMenuItem("Blue");
		itemGreen = new JMenuItem("Green");

		menu.add(itemRed);
		menu.add(itemBlue);
		menu.add(itemGreen);
		mainMenu.add(menu);

		// setmanuBar
		menuPanel.add(mainMenu);
		setJMenuBar(mainMenu);

	}

	public void setListener(ActionListener list) {
		itemRed.addActionListener(list);
		itemBlue.addActionListener(list);
		itemGreen.addActionListener(list);

	}

}
