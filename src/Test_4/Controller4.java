package Test_4;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;



public class Controller4 {
	ColorFrame4 frame;
	ActionListener list;

	public static void main(String[] args) {
		new Controller4();
	}

	public Controller4() {
		frame = new ColorFrame4();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 400);
		frame.setVisible(true);
		frame.setTitle("My Colors");
		frame.setLocation(510, 160);
		list = new ListenerMgr();
		frame.setListener(list);

	}

	class ListenerMgr implements ActionListener {
		Color color = frame.colorPanel.getBackground();
		@Override
		public void actionPerformed(ActionEvent e) {
			JComboBox jcm = (JComboBox) e.getSource();
			String cm = (String) jcm.getSelectedItem();
			if (cm.equals("Red"))
				 color =Color.RED;
				frame.colorPanel.setBackground(color);
			if (cm.equals("Green"))
				color = Color.GREEN;
				frame.colorPanel.setBackground(color);
		    if (cm.equals("Blue"))
				color =Color.blue; 
		    	frame.colorPanel.setBackground(color);
			

		}
	}
}


