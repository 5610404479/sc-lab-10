package Test_3;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class Controller3 {
	ColorFrame3 frame;
	ActionListener list;

	public static void main(String[] args) {
		new Controller3();
	}

	public Controller3() {
		frame = new ColorFrame3();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 400);
		frame.setVisible(true);
		frame.setTitle("My Colors");
		frame.setLocation(510, 160);
		list = new ListenerMgr();
		frame.setListener(list);

	}

	class ListenerMgr implements ActionListener {
		int c1 = 0;
		int c2 = 0;
		int c3 = 0;
		@Override
		public void actionPerformed(ActionEvent e) {		
			if (e.getSource() == frame.bRed){
			    c1 = Color.red.getRed();	   
			}
			 if (e.getSource() == frame.bGreen){
			    c2 = Color.GREEN.getGreen();	   
			}	
			if (e.getSource() == frame.bBlue){
			    c3 = Color.BLUE.getBlue();	  
			}

			frame.colorPanel.setBackground(new Color(c1, c2, c3));
			

		}
	}
}
