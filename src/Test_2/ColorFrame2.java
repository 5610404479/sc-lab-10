package Test_2;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class ColorFrame2 extends JFrame {
	JPanel colorPanel;
	private JPanel buttonPanel;
	JRadioButton bRed, bGreen, bBlue;
	private TitledBorder l1;
	private Border board;
	ButtonGroup group;

	public ColorFrame2() {
		createPanel();
		createPanelButton();

	}

	public void createPanel() {
		colorPanel = new JPanel();
		buttonPanel = new JPanel();
		add(colorPanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);

	}

	public void createPanelButton() {
		bRed = new JRadioButton("Red");
		bGreen = new JRadioButton("Green");
		bBlue = new JRadioButton("Blue");
		group = new ButtonGroup();
		buttonPanel.add(bRed);
		buttonPanel.add(bBlue);
		buttonPanel.add(bGreen);
		group.add(bRed);
		group.add(bGreen);
		group.add(bBlue);
		board = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		l1 = BorderFactory.createTitledBorder(board, "Color");
		buttonPanel.setBorder(l1);

	}

	public void setListener(ActionListener list) {
		bRed.addActionListener(list);
		bGreen.addActionListener(list);
		bBlue.addActionListener(list);

	}
}