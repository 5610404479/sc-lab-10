package Test_6;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class view extends JFrame {
    JPanel inputPanel,outPanel;
	JButton btWithdraw,btDeposit;
	JTextField text1;
	JLabel show;
	private TitledBorder l1,l2;
	private Border board1,board2,raisedbevel ;
	private String str ;

	public view() {
		createPanel();
		createPanelinput();
		createPaneloutput();
		
	}

	public void createPanel() {
		inputPanel = new JPanel();
		outPanel = new JPanel();
		add(inputPanel, BorderLayout.CENTER);
		add(outPanel, BorderLayout.SOUTH);
		
	}
	
	public void createPanelinput() {
		text1 = new JTextField(20);
		btWithdraw = new JButton("Withdraw");
		btDeposit = new JButton("Deposite");
		
		board1 = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		l1 = BorderFactory.createTitledBorder(board1, "Input Money");
		inputPanel.setBorder(l1);
		inputPanel.add(text1);
		inputPanel.add(btDeposit);
		inputPanel.add(btWithdraw);
	
		
	}
	public void createPaneloutput(){
		show = new JLabel();
		board2 = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		l2 = BorderFactory.createTitledBorder(board2, "Balance");
		outPanel.setBorder(l2);
		outPanel.add(show);
		raisedbevel = BorderFactory.createRaisedBevelBorder();
		
		show.setBorder(raisedbevel);
		
	}
	public void setText(String str){
		this.str = str ;
		show.setText(str);
	}

	public void setListener(ActionListener list) {
		btWithdraw.addActionListener(list);
		btDeposit.addActionListener(list);
		

	}

	
	

	
}
